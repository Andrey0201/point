import controller.PointController;

import model.Circle;
import model.Point;
import model.PointList;
import units.Validator;
import view.View;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        ArrayList<Point> points = new ArrayList<>();
        Point point = new Point();
        Validator validator = new Validator();
        Circle circle = new Circle();
        View view = new View(point, validator, circle);
        PointList pointList = new PointList(points, view, circle);


        PointController pointController = new PointController(view, pointList);
        pointController.runApp();


    }
}
